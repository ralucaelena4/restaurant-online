﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_3_Mvp
{
    public class Client
    {
        private int id;
        private string name;
        private string email;
        private string telephone;
        private string address;
        private string password;
        private bool active;

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Telephone { get => telephone; set => telephone = value; }
        public string Address { get => address; set => address = value; }
        public string Password { get => password; set => password = value; }
        public bool Active { get => active; set => active = value; }
        public string Email { get => email; set => email = value; }
    }
}
