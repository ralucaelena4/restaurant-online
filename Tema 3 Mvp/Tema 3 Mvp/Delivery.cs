﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_3_Mvp
{
    class Delivery
    {
        private int id;
        private int clientId;

        public int Id { get => id; set => id = value; }
        public int ClientId { get => clientId; set => clientId = value; }
    }
}
