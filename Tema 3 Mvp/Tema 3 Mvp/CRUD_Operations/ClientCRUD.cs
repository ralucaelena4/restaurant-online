﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
using System.Configuration;

namespace Tema_3_Mvp
{
    class ClientCRUD
    {
        public static void InsertClient(string name, string email, string telephone, string addres, string password)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spAddClient";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@new_name", name);
                        command.Parameters.AddWithValue("@new_email", email);
                        command.Parameters.AddWithValue("@new_telephone", telephone);
                        command.Parameters.AddWithValue("@new_address", addres);
                        command.Parameters.AddWithValue("@new_password", password);
                        command.Parameters.AddWithValue("@new_active", true);
                        command.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    { }
                }

            }
        }

        public static ObservableCollection<Client> SelectClient()
        {
            ObservableCollection<Client> clients = new ObservableCollection<Client>();
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spClientActiveSelect";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Client client = new Client();
                            client.Id = reader.GetInt32(0);
                            client.Name = reader.GetString(1);
                            client.Email = reader.GetString(2);
                            client.Telephone = reader.GetString(3);
                            client.Address = reader.GetString(4);
                            client.Password = reader.GetString(5);
                            clients.Add(client);
                            command.ExecuteNonQuery();
                        }
                        reader.Close();
                    }
                    catch (Exception e)
                    { }
                }
            }
            return clients;
        }

        static public Client GetClientBasedOnEmail(string email, string password)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spClientActiveSelect";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            if (reader.GetString(2) == email)
                            {
                                if (reader.GetString(5) == password)
                                {
                                    Client client = new Client();
                                    client.Id = reader.GetInt32(0);
                                    client.Name = reader.GetString(1);
                                    client.Email = reader.GetString(2);
                                    client.Telephone = reader.GetString(3);
                                    client.Address = reader.GetString(4);
                                    client.Password = reader.GetString(5);
                                    return client;

                                }
                            }
                        }
                    }
                    catch (Exception e) { }
                }
                return null;
            }
        }

    }
}
