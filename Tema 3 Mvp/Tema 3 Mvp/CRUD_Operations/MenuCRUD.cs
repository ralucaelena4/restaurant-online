﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
using System.Configuration;

namespace Tema_3_Mvp
{
    class MenuCRUD
    {
        static public ObservableCollection<Menu> SelectMenu()
        {
            ObservableCollection<Menu> menus = new ObservableCollection<Menu>();
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spSelectActiveMenu";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Menu menu = new Menu();
                            menu.Id = reader.GetInt32(0);
                            menu.Name = reader.GetString(1);
                            menu.TypeId = reader.GetInt32(2);
                            menu.Active = reader.GetBoolean(3);
                            menus.Add(menu);
                        }
                        reader.Close();
                    }
                    catch (Exception e) { }
                }
            }
            return menus;
        }

        static public Menu GetMenuBasedOnName(string name)
        {
            Menu menu = new Menu();
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spSelectActiveMenu";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            if (name == reader.GetString(1))
                            {
                                menu.Id = reader.GetInt32(0);
                                menu.Name = reader.GetString(1);
                                menu.TypeId = reader.GetInt32(2);
                                menu.Active = reader.GetBoolean(3);
                                return menu;
                            }
                        }
                        reader.Close();
                    }
                    catch (Exception e) { }
                }
            }
            return null;        }

        static public void InsertMenu(int typeId)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spAddMenu";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@new_typeId", typeId);
                        command.Parameters.AddWithValue("@new_active", true);
                        command.ExecuteNonQuery();
                    }
                    catch (Exception e) { }
                }
            }
        }

        static public void InsertMenuDelivery(int menuId, int deliveryId, int quantity)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spAddMenuDelivery";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@new_menuId", menuId);
                        command.Parameters.AddWithValue("@new_deliveryId", deliveryId);
                        command.Parameters.AddWithValue("@new_quantity", quantity);
                        command.ExecuteNonQuery();
                    }
                    catch (Exception e) { }
                }
            }
        }

        static public void InsertMenuProduct(int menuId, int productId, int quantity)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spAddMenuDelivery";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@new_menuId", menuId);
                        command.Parameters.AddWithValue("@new_productId", productId);
                        command.Parameters.AddWithValue("@new_quantity", quantity);
                        command.ExecuteNonQuery();
                    }
                    catch (Exception e) { }
                }
            }
        }

        static public ObservableCollection<int> GetMenuProducts(int menuId)
        {
            ObservableCollection<int> productsId = new ObservableCollection<int>();
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spAddMenuProduct";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@new_menuId", menuId);

                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            int id;
                            id = reader.GetInt32(0);
                            productsId.Add(id);
                        }
                    }
                    catch (Exception e) { }
                }
            }
            return productsId;
        }

        static public ObservableCollection<Product> PopulateMenuProducts(Menu menu)
        {
            ObservableCollection<Product> products = new ObservableCollection<Product>();
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spGetMenuProductsBasedOnId";
                    SqlCommand command = new SqlCommand(commandString, connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@idMenu", menu.Id);
                    try
                    {
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Product product = new Product();
                            product = ProductCRUD.GetProduct(reader.GetInt32(0));
                            products.Add(product);
                        }

                    }
                    catch (Exception e) { }
                }
            }
            return products;
        }
    }
}
