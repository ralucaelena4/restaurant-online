﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
using System.Configuration;

namespace Tema_3_Mvp
{
    class ProductCRUD
    {
        static public void InsertProduct(string name, float price, int quantity, int totalQuantity, bool active)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spClientInsert";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@new_name", name);
                        command.Parameters.AddWithValue("@new_price", price);
                        command.Parameters.AddWithValue("@new_quantity", quantity);
                        command.Parameters.AddWithValue("@new_totalQuantity", totalQuantity);
                        command.Parameters.AddWithValue("@new_active", active);
                        command.Parameters.AddWithValue("@new_typeId", true);
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        //log
                    }
                    catch (Exception e)
                    {
                        //logger
                    }
                }
            }
        }

        public static void GetProductType(ref Product product)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spGetProductType";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@typeId", product.Id);

                        SqlDataReader reader = command.ExecuteReader();
                        if (reader.Read())
                            product.Type = reader.GetString(0);

                        reader.Close();
                    }
                    catch (Exception e)
                    { }
                }
            }
        }

        public static void InsertProductDelivery(int productId, int deliveryId, int quantity)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spAddProductDelivery";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@new_productId", productId);
                        command.Parameters.AddWithValue("@new_deliveryId", deliveryId);
                        command.Parameters.AddWithValue("@new_quantity", quantity);
                        command.ExecuteNonQuery();
                    }
                    catch (Exception e) { }
                }
            }
        }

        static public ObservableCollection<Product> SelectActiveProducts()
        {
            ObservableCollection<Product> products = new ObservableCollection<Product>();
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spProductActiveSelect";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Product product = new Product();
                            product.Id = reader.GetInt32(0);
                            product.Name = reader.GetString(1);
                            product.Price = reader.GetDouble(2);
                            product.Quantity = reader.GetInt32(3);
                            product.TotalQuantity = reader.GetInt32(4);
                            product.Active = reader.GetBoolean(5);
                            product.TypeId = reader.GetInt32(6);
                            GetProductType(ref product);
                            products.Add(product);
                        }

                        reader.Close();

                    }
                    catch (Exception e)
                    { }

                }
            }
            return products;
        }

        static public ObservableCollection<Product> GroupByType(string type)
        {
            ObservableCollection<Product> products = new ObservableCollection<Product>();
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spGetProductsGroupByType";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@type", type);
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Product product = new Product();
                            product.Id = reader.GetInt32(0);
                            product.Name = reader.GetString(1);
                            product.Price = reader.GetDouble(2);
                            product.Quantity = reader.GetInt32(3);
                            product.TotalQuantity = reader.GetInt32(4);
                            product.Active = reader.GetBoolean(5);
                            product.TypeId = reader.GetInt32(6);
                            GetProductType(ref product);
                            products.Add(product);
                        }

                        reader.Close();

                    }
                    catch (Exception e)
                    { }

                }
            }
            return products;
        }

        public static Product GetProduct(int index)
        {
            Product product = new Product();
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spGetProduct";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@index", index);

                        SqlDataReader reader = command.ExecuteReader();
                        if (reader.Read())
                        {
                            product.Id = reader.GetInt32(0);
                            product.Name = reader.GetString(1);
                            product.Price = reader.GetDouble(2);
                            product.Quantity = reader.GetInt32(3);
                            product.TotalQuantity = reader.GetInt32(4);
                            product.Active = reader.GetBoolean(5);
                            product.TypeId = reader.GetInt32(6);
                            GetProductType(ref product);
                        }

                        reader.Close();
                    }
                    catch (Exception e)
                    { }
                }
            }
            return product;
        }

        static public ObservableCollection<string> SelectTypes()
        {
            ObservableCollection<string> types = new ObservableCollection<string>();
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spSelectProductType";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            types.Add(reader.GetString(0));
                        }

                        reader.Close();

                    }
                    catch (Exception e)
                    { }

                }
            }
            return types;

        }

    }
}


