﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_3_Mvp
{
    class DeliveryCRUD
    {
        static public void InsertDelivery(int clientId, string paymentType)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spAddDelivery";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@new_clientId", clientId);
                        command.Parameters.AddWithValue("@new_paymentType", paymentType);
                        command.ExecuteNonQuery();
                    }
                    catch (Exception e) { }
                }
            }
        }


        static public int getLastDeliveryId(int clientId)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("ConnectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spLastDeliveryForClientId";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@clientId", clientId);
                        SqlDataReader reader = command.ExecuteReader();
                        if (reader.Read())
                        {
                            int id = reader.GetInt32(0);
                            return id;
                        }
                    }
                    catch (Exception e) { }
                }
            }
            return 0;
        }

    }
}

