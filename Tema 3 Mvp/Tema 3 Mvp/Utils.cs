﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_3_Mvp
{
    class Utils
    {
        static public bool VerifyEmail(string email)
        {
            ObservableCollection<Client> clients = ClientCRUD.SelectClient();
            foreach (Client client in clients)
            {
                if (client.Email == email)
                    return false;
            }
            return true;
        }

        static public ObservableCollection<string> MenuTypes()
        {
            ObservableCollection<string> types = new ObservableCollection<string>();
            ObservableCollection<Menu> menus = MenuCRUD.SelectMenu();
            foreach(Menu menu in menus)
            {
                types.Add(menu.Name);
            }
            return types;
        }

        static public Menu GetMenuBasedOnName(string name)
        {
            ObservableCollection<Menu> menus = MenuCRUD.SelectMenu();
            foreach(Menu menu in menus)
            {
                if (menu.Name == name)
                    return menu;
            }
            return null;
        }

    }
}
