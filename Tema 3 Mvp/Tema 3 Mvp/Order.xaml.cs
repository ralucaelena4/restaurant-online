﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Tema_3_Mvp
{
    public partial class Order : Window
    {
        private ObservableCollection<Product> products = new ObservableCollection<Product>();
        private ObservableCollection<Menu> menus = new ObservableCollection<Menu>();
        Client client = new Client();

        public Order(Client client, ObservableCollection<Product> products, ObservableCollection<Menu> menus)
        {
            InitializeComponent();
            this.products = products;
            this.menus = menus;
            dataGridProducts.ItemsSource = products;
            dataGridMenus.ItemsSource = menus;
            this.client = client;
            GetPrice();
        }

        private void DataGrid_OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName == "Id" || e.PropertyName == "TotalQuantity" || e.PropertyName == "Active" || e.PropertyName == "TypeId" || 
                e.PropertyName == "ProductsId" || e.PropertyName == "Products")
            {
                e.Column = null;
            }
        }

        private void btnDeleteProduct_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridProducts.SelectedItem != null)
            {
                Product product = (Product)dataGridProducts.SelectedItem;
                products.Remove(product);
                dataGridProducts.ItemsSource = products;
                GetPrice();
            }
        }
        private void btnDeleteMenu_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridMenus.SelectedItem != null)
            {
                Menu menu = (Menu)dataGridMenus.SelectedItem;
                menus.Remove(menu);
                dataGridMenus.ItemsSource = menus;
                GetPrice();
            }
        }


        private void btnAddOrder_Click(object sender, RoutedEventArgs e)
        {
            if (comboBox.SelectedItem != null)
            {
                DeliveryCRUD.InsertDelivery(client.Id, comboBox.Text);
                int deliveryId = DeliveryCRUD.getLastDeliveryId(client.Id);
                foreach (Product product in products)
                {
                    ProductCRUD.InsertProductDelivery(product.Id, deliveryId, 1);
                }
                foreach (Menu menu in menus)
                {
                    MenuCRUD.InsertMenuDelivery(menu.Id, deliveryId, 1);
                }
                txtBlockMessage.Text = "Comanda trimisa!";
            }
            else
            {
                txtBlockMessage.Text = "Nu ati ales metoda de plata!";
            }

        }

        private void GetPrice()
        {
            double price = 0;
            foreach (Product product in products)
            {
                price += product.Price;
            }
            foreach (Menu menu in menus)
            {
                foreach (Product product in menu.Products)
                {
                    price += product.Price;
                }
            }

            txtBlockPrice.Text = price.ToString();
        }

        private void comboBoxAddPayment_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }


    }
}
