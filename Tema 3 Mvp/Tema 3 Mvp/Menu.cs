﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_3_Mvp
{
    public class Menu
    {
        private int id;
        private string name;
        private int typeId;
        private bool active;

        private ObservableCollection<int> productsId;
        private ObservableCollection<Product> products;

        public int Id { get => id; set => id = value; }
        public int TypeId { get => typeId; set => typeId = value; }
        public bool Active { get => active; set => active = value; }
        public string Name { get => name; set => name = value; }
        public ObservableCollection<int> ProductsId { get => productsId; set => productsId = value; }
        public ObservableCollection<Product> Products { get => products; set => products = value; }
    }
}
