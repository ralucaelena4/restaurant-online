﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_3_Mvp
{
    public class Product
    {
        private int id;
        private string name;
        private double price;
        private int quantity;
        private int totalQuantity;
        private int typeId;
        private string type;
        private bool active;

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public double Price { get => price; set => price = value; }
        public int Quantity { get => quantity; set => quantity = value; }
        public int TotalQuantity { get => totalQuantity; set => totalQuantity = value; }
        public int TypeId { get => typeId; set => typeId = value; }
        public bool Active { get => active; set => active = value; }
        public string Type { get => type; set => type = value; }
    }
}
