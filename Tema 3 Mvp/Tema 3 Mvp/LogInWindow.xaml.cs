﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Tema_3_Mvp
{
    public partial class LogInWindow : Window
    {
        private Client client = new Client();
        public LogInWindow()
        {
            InitializeComponent();
        }

        private void btnAddClien_Clickt(object sender, RoutedEventArgs e)
        {
            if(txtAddName.Text == null || txtAddEmail.Text=="" || txtAddAddress.Text == "" || txtAddTelephone.Text == "" || txtAddTelephone.Text == "" || txtAddPassword.Password == "")
            {
                txtBlockMessage.Text = "Nu ati adaugat completat toate campurile necesare!";
                return;
            }
            if (Utils.VerifyEmail(txtAddEmail.Text))
            {
                ClientCRUD.InsertClient(txtAddName.Text, txtAddEmail.Text, txtAddTelephone.Text, txtAddAddress.Text, txtAddPassword.Password);
                txtBlockMessage.Text = "Adaugare reusita!";
            }
            else
            {
                txtBlockMessage.Text = "Exista deja un cont pentru acest email!";
            }
        }

        private void btnLogIn_Click(object sender, RoutedEventArgs e)
        {
            if(txtLogInEmail.Text =="" || txtLogInPassword.Password =="")
            {
                txtBlockMessage.Text = "Nu ati adaugat completat toate campurile necesare!";
                return;
            }
            Client client = ClientCRUD.GetClientBasedOnEmail(txtLogInEmail.Text, txtLogInPassword.Password);
            if (client != null)
            {
                txtBlockMessage.Text = "Autentificare reusita!";
                this.client = client;
            }
            else
            {
                txtBlockMessage.Text = "Email-ul si parola nu corespund unui cont deja existent!";
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            MainWindow wind = new MainWindow(client);
            wind.Show();
            this.Close();
        }
    }
}
