﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tema_3_Mvp
{
    public partial class MainWindow : Window
    {
        private Client client= new Client();
        private ObservableCollection<Product> orderProducts = new ObservableCollection<Product>();
        private ObservableCollection<Menu> orderMenus = new ObservableCollection<Menu>();

        public MainWindow(Client client)
        {
            InitializeComponent();
            this.client = client;
            ObservableCollection<string> menuTypes = Utils.MenuTypes();
            comboBoxMenus.ItemsSource = menuTypes;
            ObservableCollection<string> types = ProductCRUD.SelectTypes();
            comboBoxProducts.ItemsSource = types;
            txtBlockCurrentUser.Text = client.Email;
        }

        public MainWindow()
        {
            InitializeComponent();
            ObservableCollection<string> menuTypes = Utils.MenuTypes();
            comboBoxMenus.ItemsSource = menuTypes;
            ObservableCollection<string> types = ProductCRUD.SelectTypes();
            comboBoxProducts.ItemsSource = types;
            txtBlockMessage.Text = client.Email;
        }

        private void MenuItemProducts_Click(object sender, RoutedEventArgs e)
        {
            ObservableCollection<Product> products = ProductCRUD.SelectActiveProducts();
            dataGrid.ItemsSource = products;
        }
        private void MenuItemMenu_Click(object sender, RoutedEventArgs e)
        {
            ObservableCollection<Menu> menus = MenuCRUD.SelectMenu();
            dataGrid.ItemsSource = menus;
        }

        private void DataGrid_OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName == "Id" || e.PropertyName == "TotalQuantity" || e.PropertyName == "Active" || e.PropertyName == "TypeId")
            {
                e.Column = null;
            }
        }

        private void MenuItemLogIn_Click(object sender, RoutedEventArgs e)
        {
            LogInWindow logIn = new LogInWindow();
            logIn.Show();
            this.Close();
        }

        private void MenuItemAddOrder_Click(object sender, RoutedEventArgs e)
        {
            if (client.Email != null)
            {
                Order wind = new Order(client, orderProducts, orderMenus);
                wind.Show();
                this.Close();
            }
            else
            {
                txtBlockMessage.Text = "Trebuie sa va logati pentru a finaliza o comanda!";
            }
        }

        private void comboBoxMenus_SelectionChangeCommitted(object sender, SelectionChangedEventArgs e)
        {
            ComboBox senderComboBox = (ComboBox)sender;
            Menu menu = Utils.GetMenuBasedOnName(senderComboBox.SelectedItem.ToString());
            menu.Products = MenuCRUD.PopulateMenuProducts(menu);
            dataGrid.ItemsSource = menu.Products;

        }

        private void comboBoxProducts_SelectionChangeCommitted(object sender, SelectionChangedEventArgs e)
        {
            ComboBox senderComboBox = (ComboBox)sender;
            ObservableCollection<Product> products = ProductCRUD.GroupByType(senderComboBox.SelectedItem.ToString());
            dataGrid.ItemsSource = products;
        }

        private void btnMenuDelivery_Click(object sender, RoutedEventArgs e)
        {
            Menu menu = new Menu();
            if(this.client.Email == null)
            {
                txtBlockMessage.Text = "Trebuie sa va logati pentru a putea plasa o comanda.";
                return;
            }
            if(comboBoxMenus.SelectedItem == null)
            {
                txtBlockMessage.Text = "Nu ati selectat nici un meniu. Trebuie sa il selectati din casuta de sus numita 'Afisati Meniurile'.";
                return;
            }
            menu = MenuCRUD.GetMenuBasedOnName(comboBoxMenus.SelectedItem.ToString());
            menu.Products = MenuCRUD.PopulateMenuProducts(menu);
            orderMenus.Add(menu);
            txtBlockMessage.Text = "Meniu adaugat cu succes!";
        }

        private void btnProductDelivery_Click(object sender, RoutedEventArgs e)
        {
            if (this.client.Email == null)
            {
                txtBlockMessage.Text = "Trebuie sa va logati pentru a putea plasa o comanda.";
                return;
            }
            Product product = (Product)dataGrid.SelectedItem;
            orderProducts.Add(product);
            txtBlockMessage.Text = "Produs adaugat cu succes!";
        }
    }
}
